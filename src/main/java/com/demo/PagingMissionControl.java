package com.demo;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * Ingest status telemetry data and create alert messages for the following violation conditions:

- If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.
- If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
 * @author Terry Peacemaker
 * 
 */
public class PagingMissionControl {
	private static final Logger logger = LoggerFactory.getLogger(PagingMissionControl.class);
	private List<Alert> alertsList = new ArrayList<Alert>();
	private List<TelemetryData> telemetryDataList = new ArrayList<TelemetryData>();
	private List<List<TelemetryData>> allTelemetryData = new ArrayList<List<TelemetryData>>();
	private Map<Long, List<TelemetryData>> mapTelemetryData = new HashMap<Long, List<TelemetryData>>();
	
	public static final int NUM_ELEMENTS = 8;
	public static final long TIME_TRESHOLD = 5000*60*5;
	public static final int NUM_READINGS_THRESHOLD = 3;
	public static final String BATT = "BATT";
	public static final String TSTAT = "TSTAT";
	
	public List<List<TelemetryData>> getAllTelemetryData() {
		return allTelemetryData;
	}
	public void setAllTelemetryData(List<List<TelemetryData>> allTelemetryData) {
		this.allTelemetryData = allTelemetryData;
	}

	private ObjectMapper mapper = new ObjectMapper();
	
	public PagingMissionControl() {		
	}
	
	public static void main(String[] args) {
		if (args.length > 0) {
            logger.info("List of arguments: {}", Arrays.toString(args));
        } else {
        	System.out.println("No file argument given. Please include the file path");
			logger.error("No file argument given. Please include the file path");
			return;
        }
		logger.info("processing for " + args[0]);
		PagingMissionControl control =  new PagingMissionControl();
		control.processTelemetryData(args);
	}
	
	public boolean processTelemetryData(String[] args) {
		boolean success = false;
		logger.info("Starting Telemetry Processing for "+ args[0]);
		try  
		{  
			File file=new File(args[0]);   
			FileReader fr=new FileReader(file); 
			BufferedReader br=new BufferedReader(fr);  
			StringBuffer sb=new StringBuffer();  
			String line;  
			while((line=br.readLine())!=null)  
			{  
				sb.append(line);      //appends line to string buffer  
				//sb.append("\n");     //line feed  
				addToDataMap(sb.toString());
				sb.setLength(0);
			}
			
			fr.close();    //closes the stream and release the resources  
	
		}  
		catch(IOException e)  
		{  
			e.printStackTrace();
			return false;
		}  
		
		
		
		
		// output the AlertList to json
		//System.out.println(convertToJson(this.alertsList));
		processAlerts();
		success = true;
		return success;
	}
	
	public void processAlerts() {
		// get separate lists from map
		Set<Long> ks = this.mapTelemetryData.keySet();
		for( Long k : ks) {
			List<TelemetryData> list = this.mapTelemetryData.get(k);
			sortTelemetryData(list);
			createAlerts(list);
		}
		outputAlerts();
		
	}
	
	public void createAlerts(List<TelemetryData> tList) {
		List<TelemetryData> tempList = new ArrayList<TelemetryData>();
		List<TelemetryData> battList = new ArrayList<TelemetryData>();
		
		// go through the telemetry data and add all the violations by making AlertDatas and adding them to the Alert list
		for(TelemetryData td : tList ) {
			if(td.getComponent().equals(PagingMissionControl.TSTAT)) {
				tempList.add(td);
			}
			if(td.getComponent().equals(PagingMissionControl.BATT)) {
				battList.add(td);
			}
		}
		
		addTempAlerts(tempList);
		addBattAlerts(battList);

	}
	
	// If for the same satellite there are three thermostat readings that exceed the red high limit within a five minute interval.
	public void addTempAlerts(List<TelemetryData> list ) {
		logger.info("addTempAlerts");
		List<TelemetryData> highTempList = new ArrayList<TelemetryData>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSSZ");
		SimpleDateFormat formatterOut = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		for(int i = 0; i < list.size();i++ ) {
			int startIndex = i;
			TelemetryData td = list.get(i);
			if( td.getRawValue() > td.getRedHighLimit() ) {
				highTempList.add(td);
				
				if( highTempList.size() == NUM_READINGS_THRESHOLD) {
					long diff = highTempList.get(highTempList.size()-1).getTimestamp().getTime() - highTempList.get(0).getTimestamp().getTime();
					logger.info("Date for last element = " + formatter.format(highTempList.get(highTempList.size()-1).getTimestamp()));
					logger.info("Date for 1st element = " + formatter.format(highTempList.get(0).getTimestamp()));
					
					logger.info("Difference in TSTAT time for id : " + td.getSatelliteId() + " = " + diff);
					if(highTempList.get(highTempList.size()-1).getTimestamp().getTime() - highTempList.get(0).getTimestamp().getTime() < TIME_TRESHOLD) {  
						Alert alert = new Alert(); 
						alert.setSatelliteId(td.getSatelliteId());
						alert.setSeverity("RED_HIGH");
						alert.setComponent(td.getComponent());
						alert.setTimestamp(formatterOut.format(highTempList.get(0).getTimestamp()));
						logger.info("Adding Temp Alert : " + alert);
						this.alertsList.add(alert);
						highTempList.clear();
						startIndex+=1;
						i = startIndex;
					}
				}
		
			}
	
		}
		
	}
	
	// - If for the same satellite there are three battery voltage readings that are under the red low limit within a five minute interval.

	public void addBattAlerts(List<TelemetryData> list ) {
		logger.info("addBattAlerts");
		List<TelemetryData> lowBattList = new ArrayList<TelemetryData>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		SimpleDateFormat formatterOut = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		
		for(int i = 0; i < list.size();i++ ) {
			int startIndex = i;
			TelemetryData td = list.get(i);
			if( td.getRawValue() < td.getRedLowLimit() ) {
				lowBattList.add(td);
				if( lowBattList.size() == NUM_READINGS_THRESHOLD) {
					long diff = lowBattList.get(lowBattList.size()-1).getTimestamp().getTime() - lowBattList.get(0).getTimestamp().getTime();
					logger.info("Date for last element = " + formatter.format(lowBattList.get(lowBattList.size()-1).getTimestamp()));
					logger.info("Date for 1st element = " + formatter.format(lowBattList.get(0).getTimestamp()));
					
					
					logger.info("Difference in Batt time for id : " + td.getSatelliteId() + " = " + diff);
					if(lowBattList.get(lowBattList.size()-1).getTimestamp().getTime() - lowBattList.get(0).getTimestamp().getTime() < TIME_TRESHOLD) { 
						Alert alert = new Alert(); 
						alert.setSatelliteId(td.getSatelliteId());
						alert.setSeverity("RED LOW");
						alert.setComponent(td.getComponent());
						alert.setTimestamp(formatterOut.format(lowBattList.get(0).getTimestamp()));
						logger.info("Adding Batt Alert : " + alert);
						this.alertsList.add(alert);
						lowBattList.clear();
						startIndex+=1;
						i = startIndex;
					}
				}
			}
		}
	}
	
	public void outputAlerts() {
		
		String strAlerts = this.convertToJson(alertsList);
		logger.info("Outputting alerts: " + strAlerts);
		System.out.print(strAlerts);
	}

	public void addToDataMap(String line) {
		String[] strList = line.split("\\|");

		// 20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
		TelemetryData td = new TelemetryData();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		
		try {
			td.setTimestamp(formatter.parse(strList[0]));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		td.setSatelliteId(Long.valueOf(strList[1]));
		td.setRedHighLimit(Double.valueOf(strList[2]));
		td.setYellowHighLimit(Double.valueOf(strList[3]));
		td.setYellowLowLimit(Double.valueOf(strList[4]));
		td.setRedLowLimit(Double.valueOf(strList[5]));
		td.setRawValue(Double.valueOf(strList[6]));
		td.setComponent(strList[7]);
		
		List<TelemetryData> tList = mapTelemetryData.get(td.getSatelliteId());
		if( tList != null ) {
			tList.add(td);
		} else {
			List<TelemetryData> list = new ArrayList<TelemetryData>();
			list.add(td);
			mapTelemetryData.put(td.getSatelliteId(), list);
		}
	}
	
	public String convertToJson(List<?> list) {
		String output = "";
		
		try {
			output = mapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			logger.error("Error processing list to json: " + e.getMessage());
			e.printStackTrace();
		}
		return output;
	}
	
	public void sortTelemetryData(List<TelemetryData> listTd) {
		Collections.sort(listTd, new Comparator<TelemetryData>() {  
		    @Override  
		    public int compare(TelemetryData p1, TelemetryData p2) {  
		        return new CompareToBuilder().append(p1.getSatelliteId(), p2.getSatelliteId()).append(p1.getTimestamp(), p2.getTimestamp()).toComparison();  
		    }  
		});  
	}
	
	
	
}
