package com.demo;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class TelemetryData {
	private Date timestamp;
	private Long satelliteId;
	private Double redHighLimit;
	private Double yellowHighLimit;
	private Double yellowLowLimit;
	private Double redLowLimit;
	private Double rawValue;
	private String component;
	
public TelemetryData() { }

	public TelemetryData( Date timestamp, Long satelliteId, Double redHighLimit, Double yellowHighLimit, Double yellowLowLimit, Double redLowLimit, Double rawValue, String component ) {
		this.timestamp = timestamp;
		this.satelliteId = satelliteId;
		this.redHighLimit = redHighLimit;
		this.yellowHighLimit = yellowHighLimit;
		this.yellowLowLimit = yellowLowLimit;
		this.redLowLimit = yellowLowLimit;
		this.rawValue = rawValue;
		this.component = component;
	}
	
	@Override
    public boolean equals(Object obj) {
		return true;
       // return ((TelemetryData) obj).getS   ().equals(getName() && );
    }

	/*
	 * @Override public int compareTo(Object o) { TelemetryData e = (TelemetryData)
	 * o; //return this.getStatelliteId().compareTo(e.getStatelliteId() && );
	 * Collections.sort(reportList, Comparator.comparing(Report::getReportKey)
	 * .thenComparing(Report::getStudentNumber) .thenComparing(Report::getSchool));
	 * }
	 */
    
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public Long getSatelliteId() {
		return satelliteId;
	}
	public void setSatelliteId(Long statelliteId) {
		this.satelliteId = statelliteId;
	}
	public Double getRedHighLimit() {
		return redHighLimit;
	}
	public void setRedHighLimit(Double redHighLimit) {
		this.redHighLimit = redHighLimit;
	}
	public Double getYellowHighLimit() {
		return yellowHighLimit;
	}
	public void setYellowHighLimit(Double yellowHighLimit) {
		this.yellowHighLimit = yellowHighLimit;
	}
	public Double getYellowLowLimit() {
		return yellowLowLimit;
	}
	public void setYellowLowLimit(Double yellowLowLimit) {
		this.yellowLowLimit = yellowLowLimit;
	}
	public Double getRedLowLimit() {
		return redLowLimit;
	}
	public void setRedLowLimit(Double redLowLimit) {
		this.redLowLimit = redLowLimit;
	}
	public Double getRawValue() {
		return rawValue;
	}
	public void setRawValue(Double rawValue) {
		this.rawValue = rawValue;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	 // <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
		public enum TELEMETRY_ELEMENTS {
			TIMESTAMP,
			SATELLITE_ID,
			RED_HIGH_LIMIT,
			YELLOW_HIGH_LIMIT,
			YELLOW_LOW_LIMIT,
			RED_LOW_LIMIT,
			RAW_VALUE,
			COMPONENT
		}
}
