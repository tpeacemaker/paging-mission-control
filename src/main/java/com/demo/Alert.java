package com.demo;

import java.util.Date;

public class Alert {
	private Long satelliteId;
	private String severity;
	private String component;
	private String timestamp;
	
	public Long getSatelliteId() {
		return satelliteId;
	}
	public void setSatelliteId(Long satelliteId) {
		this.satelliteId = satelliteId;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getComponent() {
		return component;
	}
	public void setComponent(String component) {
		this.component = component;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String string) {
		this.timestamp = string;
	}
	
	@Override
	public String toString() {
		return "Alert [satelliteId=" + satelliteId + ", severity=" + severity + ", component=" + component
				+ ", timestamp=" + timestamp + "]";
	}
}
